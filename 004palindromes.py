# Largest palindrome product
# Problem 4
# Solution from J Cartwright
#
# A palindromic number reads the same both ways.
# The largest palindrome made from the product of
# two 2-digit numbers is 9009 = 91 × 99.

# Find the largest palindrome made from the product of two 3-digit numbers.

# define a function that test is a number is a palidrome
def isPalindrome(number):
    palindrome = False
    original = str(number)
    reverse = original[::-1]
    if original == reverse:
        palindrome = True
    return palindrome

# create a list of palindrome numbers
palindromes = []

for num1 in range(999,1,-1):
    for num2 in range(999,1,-1):
        product = num1 * num2
        if isPalindrome(product) and product not in palindromes:
            palindromes.append(product)

# sort the list of palindromes
palindromes.sort()

# print the last item in the list
print(palindromes[-1])
