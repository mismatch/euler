# Power digit sum
# Problem 16

# 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

# What is the sum of the digits of the number 2^1000?

n = 2**1000

numberString = str(n)

total = 0
for i in numberString:
    total += int(i)

print ("my solution is {} " .format(total))

# one line version from forum
print ("from the PE forum is", sum(map(int, list(str(2**1000)))))
