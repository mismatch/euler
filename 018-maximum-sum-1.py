# maximum path sum 1
# problem 18
# By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
#
# 3
# 7 4
# 2 4 6
# 8 5 9 3
#
# That is, 3 + 7 + 4 + 9 = 23.
#
# Find the maximum total from top to bottom of the triangle below:
#
# 75
# 95 64
# 17 47 82
# 18 35 87 10
# 20 04 82 47 65
# 19 01 23 75 03 34
# 88 02 77 73 07 63 67
# 99 65 04 28 06 16 70 92
# 41 41 26 56 83 40 80 70 33
# 41 48 72 33 47 32 37 16 94 29
# 53 71 44 65 25 43 91 52 97 51 14
# 70 11 33 28 77 73 17 78 39 68 17 57
# 91 71 52 38 17 14 91 43 58 50 27 29 48
# 63 66 04 68 89 53 67 30 73 16 69 87 40 31
# 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
#
# NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)

# i misinterpreted the instructions above to mean that you always start at the top. It is instead to find the largest total through the triangle such that you can only ever pick from the 2 options on the line below - but that does not mean that you must pick 95 on the second line! instead, you need to start from the bottom of the triangle and work up

triangle = """
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
"""

# create an array from each line, each item will be a string
lineTriangle = triangle.splitlines()

# remove the first item
lineTriangle.pop(0)

# create an empty array for the integers
intTriangle=[]

# map each of the stings to an intgeger, and create a new list of arrays
for i in lineTriangle:
    intTriangle.append(list(map(int,i.split())))

result = 0
j = 0

for i in intTriangle:
    if len(i) == 1:
        result += i[j]
        print(f'j is {j}, so add {i[j]} to make {result}')
    elif i[j+1] > i[j]:
        result += i[j+1]
        print(f'j is {j}, so add {i[j+1]} to make {result}')
        j += 1
    else:
        result += i[j]
        print(f'j is {j}, so add {i[j]} to make {result}')

print(intTriangle)
print(f'the total is {result}')
