import utils.euler as euler

def expression(n):
    return n**2 - 67*n + 1889

iPrime = []
nPrime = []
for i in range (1,200):
    p = expression(i)
    if euler.isPrime(p) is True:
        iPrime.append(i)
        # print(i, euler.isPrime(p),"\n")

for i in range (1,200):
    p = expression(i)
    if euler.isPrime(p) is False:
        nPrime.append(i)
        # print(i, euler.isPrime(p),"\n")

print(iPrime)
print(nPrime)

