# Largest prime factor
# Problem 3

# What is the largest prime factor of the number 600851475143 ?

import utils.euler as euler

# more compact version of above
# def is_prime(n):
#     if n % 2 == 0 and n > 2:
#         return False
#     return all(n % i for i in range(3, int(n**0.5) + 1, 2))


def maxPrimeFactor(x):
    result = []
    for i in range(2, x):
        # check if the number is both prime and a factor, then add all prime
        # factors to the array
        if euler.isPrime(i) and x % i == 0:
            result.append(i)
            # print(result)
    # print the last number in the array
    print(result[-1])

# slight refinement without using an array


def maxPrimeFactor2(x):
    for i in range(2, x):
            # check if the number is both prime and a factor, then set the
            # result to the prime factor
        if euler.isPrime(i) and x % i == 0:
            result = i
            # print(result)
    print(result)

# fastest so far
#   n = 600851475143
#   i = 2
# while i * i < n:
#     while n % i == 0:
#         n = n // i
#     i = i + 1
#
# print (n)

# maxPrimeFactor2(600851475143)
maxPrimeFactor2(100)
