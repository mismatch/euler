# 10001st prime
# Problem 7

# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can
# see that the 6th prime is 13.

# What is the 10 001st prime number?

import utils.euler as euler

primes = []
i = 2
while len(primes) < 10001:
    if euler.isPrime(i):
        primes.append(i)
    i += 1

print(primes[-1])
