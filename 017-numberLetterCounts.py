# number letter counts
# problem 17

# If the numbers 1 to 5 are written out in words: one, two, three, four, five,
#  then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

# If all the numbers from 1 to 1000 (one thousand) inclusive were written out
# in words, how many letters would be used?

# NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
# forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20
# letters. The use of "and" when writing out numbers is in compliance with
# British usage.

###    functions    ###
def numWord( number ) :
    # convert upto a 4-digit integer to words
    units = ["", "one", "two", "three", "four",
            "five", "six", "seven", "eight", "nine" ]
    teens = ["", "eleven", "twelve", "thirteen", "fourteen",
             "fifteen", "sixteen", "seventeen", "eighteen",
             "nineteen" ]
    tens = ["", "ten", "twenty", "thirty", "forty", "fifty",
            "sixty", "seventy", "eighty", "ninety" ]
    # hundreds uses units class plus "hundred"
 
    # numWord the number to a string that is always 4 digits long
    # the :0n puts zeros as placeholders for shorter numbers
    snum = f'{number:04}'
    d0 = int(snum[0]) # thousands digit
    d1 = int(snum[1]) # hundreds digit
    d2 = int(snum[2]) # tens digit
    d3 = int(snum[3]) # units digit

    # th is 'n thousands'
    if d0 != 0 :
        th = f'{units[d0]} thousand '
    else :
        th = ''

    # hu is 'm hundred' 
    if d1 != 0 :
        hu = f'{units[d1]} hundred '
    else :
        hu = ''
    
    # last two digits
    if d2 == 1 and d3 != 0 :
        last = teens[d3]
    elif d2==0:
        last = units[d3]
    else:
        last = tens[d2] + ' ' + units[d3]

    # numbers like 9000 or 9000
    if number % 10 == 0 and d2==0 and d3==0:
        return th + hu

    # numbers like 56
    if d0==0 and d1==0:
        return last

    # numbers like 3045
    # if d0 != 0 and d2==0:
        # return th + 'and ' + last

    # if d0==0 and d1!=0:
        # return hu + 'and ' + last

    return th + hu + 'and ' + last

# print(numWord(123))
# print(numWord(13))
# print(numWord(910))
# print(numWord(406))
# print(numWord(1486))
# print(numWord(7832))
# print(numWord(3900))
# print(numWord(4057))

# test numbers
for i in range (1,1000):
    print(numWord(i))

# final solution
total = 0
for i in range (1,1001):
    total +=(len(numWord(i).replace(' ','')))

print(f'the total of all of the letters in the first 1000 words is {total}')

