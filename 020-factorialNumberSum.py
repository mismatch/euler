import utils.euler as euler

x = 100
print (euler.factorial(x))

def sumFactorial(number):
    total = 0
    fnumber = euler.factorial(number)
    while fnumber > 0:
        last = fnumber % 10
        total += last
        fnumber = fnumber//10
    return total

def sumDigits(number):
    # recursive function for summing digits
    return 0 if number == 0 else int(number % 10) + sumDigits(int(number / 10)) 

# this one is correct:
print(f'\nthe correct solution:\nthe sum of the digits for {x}! is {sumFactorial(x)}\n----------------------------------------')

# this does not give the same answer
# sumDigits does not seem to work for very large numbers (22! onwards)
print(f'\nthe following is an error:\nthe sum of the digits for {x}! with the recursive sumDigits is {sumDigits(euler.factorial(x))}\n----------------------------------------')
