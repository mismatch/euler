# Sum square difference
# Problem 6
#
# Find the difference between the sum of the squares of the
# first one hundred natural numbers and the square of the sum.

# formula for the sum of the first n integers
def sumSquares(n):
    return int(n**3/3 + n**2/2 +n/6)

# formula for the sum of the first n integers
def squareSum(n):
    return (int(n**2/2 +n/2))**2

def problem6(n):
    return squareSum(n)-sumSquares(n)

# test the above functions
# print ("sum of squares from 1 to 10 is", sumSquares(10))
# print ("square of the sum from 1 to 10 is", squareSum(10))

# find the solution to problem 6
print ("when n is 100:", problem6(100))
