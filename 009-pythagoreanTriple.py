# Special Pythagorean triplet
# Problem 10

# A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
# a^2 + b^2 = c^2

# For example, 32 + 42 = 9 + 16 = 25 = 52.

# There exists exactly one Pythagorean triplet for which a + b + c = 1000.
# Find the product abc.

# a very inefficient method that finds the solution
# for a in range(1,500):
#   for b in range(1,500):
#     for c in range(1,500):
#       if (a**2 + b**2 == c**2) and a+b+c == 1000:
#         print (a, b, c)
#         print ("here's one", a*b*c)
#         break
# print ("finally finished")

for a in range(3,1000):
  for b in range (a+1,1000-1-a):
    c = 1000 - a - b
    if c**2 == a**2 + b**2:
      print ("this a,b,c: ",a,b,c)
      print ("with product", a*b*c)