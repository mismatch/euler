# Summation of primes
# Problem 10

# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

# Find the sum of all the primes below two million.

import utils.euler as euler
total = 0
for i in range(2,2000000):
  if euler.isPrime(i):
    total += i
print(total)
