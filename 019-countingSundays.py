# counting Sundays
# problem 19
# you are given the following information, but you may prefer to do some research for yourself.
#
# 1 Jan 1900 was a Monday.
# thirty days has September,
# april, June and November.
# all the rest have thirty-one,
# saving February alone,
# which has twenty-eight, rain or shine.
# and on leap years, twenty-nine.
# a leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
# how many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

# list all leap years using a list comprehension
leapYears = [year for year in range(1901,2001) if year % 4 == 0 and year % 100 != 0 or year % 400 == 0]
print(leapYears)

# function to return the number of days in each of the months depending on if it is a leap year
def listMonthdays(year):
    if year in leapYears:
        monthDays = [31,29,31,30,31,30,31,31,30,31,30,31]
    else:
        monthDays = [31,28,31,30,31,30,31,31,30,31,30,31]
    return monthDays


x = 1 # 01 jan 1901
sundays = 0 # counter for the number of sundays

# 1st january 1901 was a tuesday, 
# ie x % 7 == 1 => tuesday
# so x % 7 == 6 => sunday

# loop through the first of each month within the range, checking if it is a sunday
for i in range(1901,2001):
    days = listMonthdays(i)

    for j in range(0,12):
        if x % 7 == 6 : 
            sundays +=1

        # x is the total number of days since 01 jan 1901 for the 1st of each month
        x = x + days[j]

print(f'there are {sundays} sundays on the first of the month between 1 january 1901 and 31 december 2001')


