""" functions for use in solving a range of problems from
Project Euler """


def isPrime(n):
    """ return True if n is a prime number """
    # exclude all even multiples except 2
    if n % 2 == 0 and n > 2:
        return False
    # only consider odd numbers up to root n
    for i in range(3, int(n**0.5) + 1, 2):
        if n % i == 0:
            return False
    return True


def sumSquares(n):
    """ returns the sum of the first n integers """
    return int(n**3 / 3 + n**2 / 2 + n / 6)


def product(list):
    """ return the product of a list of numbers """
    r = 1
    for n in list:
        r *= n
    return r


def triangleNumber(n):
    """ returns the nth triangle number """
    return int(n / 2 * (n + 1))


def countFactors(n):
    """ count the number of factors in a number """
    factors = 0
    for i in range(1, int(n**0.5) + 1):
        if n % i == 0:
            factors += 2
        if i * i == n:
            factors -= 1
    return factors


def countDigits(number):
    """ count the number of digits in an integer """
    count = 0
    while number >= 1:
        number = number / 10
        count += 1
    return count

def factorial(number):
    # return the factorial for a given number
    if number == 1 or number == 0:
        return 1
    else:
        return number * factorial(number - 1)
