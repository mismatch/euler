def fib(n):    # write Fibonacci series up to n
    a, b = 1, 1
    while b < n:
        print (b),
        a, b = b, a+b
        
def fib2(n): # return Fibonacci series up to n
    result = []
    a, b = 1, 1
    while b < n:
        result.append(b)
        a, b = b, a+b
    print (result)

def fib3(n): # sum even terms of the list
    result = []
    a, b = 1, 1
    while b < n:
        result.append(b)
        a, b = b, a+b
    evens1 = result[1::2] # subscripting / slice
    evens2 = [result[i] for i in range(len(result)) if i % 2 == 1] #comprehensions
    print (evens1)
    print (evens2)
    print (sum(evens2))

fib3(4000000)

## the above is not what the problem is looking for....

def fib4(n): # sum terms that are even
    result = []
    a, b = 1, 1
    while b < n:
        if b%2 == 0:
            result.append(b)
        a, b = b, a+b
    print (result)
    print (sum(result))


fib4(4000000)